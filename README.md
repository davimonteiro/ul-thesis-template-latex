# UL_PhD_Latex_Template
Latex template for PhD dissertation at University of Limerick (good for Undergraduate and Postgraduate thesis too)

Clone/Download this entire repository
---
Instructions

1. open Thesis.tex   (This is the main and only file to compile) 
2. Run "Typeset" -- "Latex"   
3. Run "Typeset" -- "BibTex"   (creates references list/ bibliography)
4. Run "Typeset" -- "Latex"    
5. Run "Typeset" -- "Latex"  


Compiled pdf offers more info.

On the folder "7_references" there is a BibTex file named "references_myphd". This files has all examples on how to cite according to the Harvard referencing style required by UL)

