\select@language {english}
\contentsline {chapter}{List of Tables}{v}{chapter*.2}
\contentsline {chapter}{List of Figures}{vii}{chapter*.3}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Aims and Objectives}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Methodology}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Research Contribution}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Thesis Outline}{2}{section.1.4}
\contentsline {chapter}{\numberline {2}Chapter 2}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Font}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Special characters}{3}{subsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.1}Subsubsection Title}{3}{subsubsection.2.1.1.1}
\contentsline {subsection}{\numberline {2.1.2}Wiki page}{3}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Another Section}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Another Subsection}{4}{subsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.1.1}Another Subsubsection Title}{4}{subsubsection.2.2.1.1}
\contentsline {chapter}{\numberline {3}Chapter 3}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Insert a Figure}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Creating a list}{6}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Creating a Table}{6}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Quote}{7}{section.3.3}
\contentsline {chapter}{\numberline {4}Math formulae in Latex}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Math}{9}{section.4.1}
\contentsline {chapter}{\numberline {5}Chapter 5}{11}{chapter.5}
\contentsline {section}{\numberline {5.1}References}{11}{section.5.1}
\contentsline {chapter}{\numberline {6}Conclusions and Future Directions}{13}{chapter.6}
\contentsline {section}{\numberline {6.1}Summary}{13}{section.6.1}
\contentsline {section}{\numberline {6.2}Conclusions}{13}{section.6.2}
\contentsline {section}{\numberline {6.3}Contributions}{13}{section.6.3}
\contentsline {section}{\numberline {6.4}Future Work}{13}{section.6.4}
\contentsline {chapter}{Appendix A: Insert figure in Appendix}{15}{figure.caption.8}
\contentsline {chapter}{Appendix B: Insert Text in Appendix}{17}{section*.10}
\contentsline {chapter}{Appendix C: Insert C++/Java et al. Code in Appendix}{21}{lstnumber.-1.143}
\contentsline {chapter}{References}{23}{appendix*.13}
